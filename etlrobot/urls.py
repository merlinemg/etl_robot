"""etlrobot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include('etl_robot_app.urls')),
url(r'^password_reset/$', auth_views.password_reset,{'template_name':'passwordreset/password_reset_form.html', 'html_email_template_name':'passwordreset/password_reset_email.html',
                   'subject_template_name':'passwordreset/password_reset_subject.txt'}, name='password_reset'),

url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,{'template_name':'passwordreset/password_reset_confirm.html'}, name='password_reset_confirm'),

url(r'^password_reset/done/$', auth_views.password_reset_done,{'template_name':'passwordreset/password_reset_done.html'}, name='password_reset_done'),

url(r'^reset/done/$', auth_views.password_reset_complete,{'template_name':'passwordreset/password_reset_complete.html'}, name='password_reset_complete'),
url(r'^auth/', include('social_django.urls', namespace='social')),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)