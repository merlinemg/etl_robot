from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode,urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import send_mail
from django.contrib.sites.models import Site
from .models import User
class SendEmail:
    def send_mail_register(user_obj):
            domain = Site.objects.get_current().domain
            subject = 'Activate Your Account'
            user = User.objects.get(id=user_obj[0].id)
            print(domain)
            # url = domain+"/reset/"+urlsafe_base64_encode(force_bytes(user.id)).decode('utf-8')+"/" +account_activation_token.make_token(user)
            domain=domain
            uid=urlsafe_base64_encode(force_bytes(user.id)).decode('utf-8')
            token = account_activation_token.make_token(user)
            message = render_to_string('emailregtemplate.html', {
            #'url': url,
            'domain':domain,
            'uid':uid,
            'token':token,
            'user':user
             })
            print (message)
            return send_mail(subject,subject, 'spericorn.test@gmail.com', [user.email], html_message=message)