from social_core.backends.google import GoogleOAuth2
from social_core.utils import handle_http_errors
from django.shortcuts import render, redirect

class CustomGoogleOAuth2(GoogleOAuth2,):
    def get_scope(self):
        scope = super(CustomGoogleOAuth2, self).get_scope()
        if self.data.get('extrascope'):
            scope = scope + ['https://www.googleapis.com/auth/bigquery','https://www.googleapis.com/auth/cloud-platform']
        else:
            scope= scope+ ['https://www.googleapis.com/auth/analytics.readonly']
        return scope

    @handle_http_errors
    def auth_complete(self,request, *args, **kwargs):
        self.process_error(self.data)
        state = self.validate_state()
        data, params = None, None
        if self.ACCESS_TOKEN_METHOD == 'GET':
            params = self.auth_complete_params(state)
        else:
            data = self.auth_complete_params(state)

        response = self.request_access_token(
            self.access_token_url(),
            data=data,
            params=params,
            headers=self.auth_headers(),
            auth=self.auth_complete_credentials(),
            method=self.ACCESS_TOKEN_METHOD
        )
        self.process_error(response)
        # self.google_essentials(response)
        # print request.session['is_google_bigquery_integration']
        request.session['google_response'] =response
        if request.session['is_google_bigquery_integration']==True:
             return redirect('etl_robot_app:googlebigquerydetails')
        elif request.session['is_google_analytics']==True:
             return redirect('etl_robot_app:googleanalyticsdetails')