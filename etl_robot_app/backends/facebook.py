from social_core.backends.facebook import FacebookOAuth2
from social_core.utils import handle_http_errors
from django.shortcuts import render, redirect

class CustomFacebookOAuth2(FacebookOAuth2):
    def get_scope(self):
        scope = super(CustomFacebookOAuth2, self).get_scope()
        if self.data.get('extrascope'):
            scope = scope + ['ads_management, ads_read, business_management, public_profile']


        else:
            scope = scope + [
                'read_insights, pages_show_list, ads_management, ads_read, business_management, public_profile']
        return scope

    @handle_http_errors
    def auth_complete(self,request, *args, **kwargs):
        self.process_error(self.data)
        state = self.validate_state()
        data, params = None, None
        if self.ACCESS_TOKEN_METHOD == 'GET':
            params = self.auth_complete_params(state)
        else:
            data = self.auth_complete_params(state)

        response = self.request_access_token(
            self.access_token_url(),
            data=data,
            params=params,
            headers=self.auth_headers(),
            auth=self.auth_complete_credentials(),
            method=self.ACCESS_TOKEN_METHOD
        )
        self.process_error(response)
        if request.session['is_facebook_ad']==True:
            request.session['facebook_response'] = response
            return redirect('etl_robot_app:fbaddetails')
        elif request.session['is_facebook_insight']==True:
            request.session['facebook_response'] = response
            return redirect('etl_robot_app:fbinsightdetails')