from django import forms
import re
from django.core.exceptions import ValidationError
from .models import User
class RegistrationForm(forms.Form):
    email =forms.EmailField(widget=forms.EmailInput(
        attrs={
        'class':'form-control',
        'id':'email',
        'onkeyup':"this.setAttribute('value', this.value);",
        'onblur' : "this.setAttribute('value', this.value);",
        'autocomplete':"off",
        #'required':'true'
        }
    ))
    first_name = forms.CharField(max_length=100,widget=forms.TextInput(
        attrs={
        'class':'form-control',
        'id':'fName',
        'onkeyup':"this.setAttribute('value', this.value);",
        'onblur' : "this.setAttribute('value', this.value);",
        'autocomplete':"off",
        #'required': 'true'
        }
    ))
    last_name =forms.CharField(max_length=200,widget=forms.TextInput(
        attrs={
        'class':'form-control',
        'id':'lName',
        'onkeyup':"this.setAttribute('value', this.value);",
        'onblur' : "this.setAttribute('value', this.value);",
        'autocomplete':"off"
        }
    ))
    company = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
        'class':'form-control',
        'id':'Company',
        'onkeyup':"this.setAttribute('value', this.value);",
        'onblur' : "this.setAttribute('value', this.value);",
        'autocomplete':"off",
       # 'required':'true'
    }
    )  )

    # policy_accept = forms.BooleanField(widget=forms.CheckboxInput(
    #     attrs={
    #         'id':'useProxy',
    #         'data - parsley - multiple' : "useProxy",
    #
    # }
    # ))

    policy_accept = forms.BooleanField()



    def clean_email(self):
        email = self.cleaned_data.get('email')
        reg = re.compile('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')

        if not reg.match(str(email)):
            raise ValidationError('Invalid Email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(u'Email "%s" is already in use.' % email)
        return self.cleaned_data.get('email')

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')

        reg = re.compile('^[a-zA-Z]*$')
        if not reg.match(str(first_name)):
            raise ValidationError('Invalid First Name')
        return self.cleaned_data.get('first_name')

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')

        reg = re.compile('^[a-zA-Z]*$')
        if not reg.match(str(last_name)):
            raise ValidationError('Invalid Last Name')
        return self.cleaned_data.get('last_name')

    def clean_company(self):
        company = self.cleaned_data.get('company')

        reg = re.compile('^[a-z0-9A-Z._]*$')
        if not reg.match(str(company)):
            raise ValidationError('Invalid Company')
        return self.cleaned_data.get('company')

    def clean_policy_accept(self):
        policy_accept = self.cleaned_data.get('policy_accept')
        if policy_accept == True:
            return self.cleaned_data.get('policy_accept')
        else:
            raise ValidationError('Accept Terms and Condition')





class FiveNineForm(forms.Form):
    email =forms.EmailField()
    password =forms.CharField(widget=forms.PasswordInput)

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if password==None:
            raise ValidationError('Password Required')
        return self.cleaned_data.get('password')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        reg = re.compile('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')

        if not reg.match(str(email)):
            raise ValidationError('Invalid Email')
        return self.cleaned_data.get('email')


class MysqlForm(forms.Form):
    schema_name = forms.CharField()
    table_prefix = forms.CharField()
    host = forms.CharField()
    port = forms.CharField()
    user = forms.CharField()
    tar_password = forms.CharField(widget=forms.PasswordInput)
    enc_type = forms.CharField(widget=forms.Select)
    ssh_port = forms.CharField(required=False)
    ssh_user = forms.CharField(required=False)
    remote_address = forms.CharField(required=False)
    public_key = forms.CharField(required=False,widget=forms.Textarea)

    def clean_schema_name(self):
        schema_name = self.cleaned_data.get('schema_name')
        reg = re.compile('^[a-z0-9A-Z_ ]*$')
        if not reg.match(str(schema_name)):
            raise ValidationError('Invalid Schema')
        return self.cleaned_data.get('schema_name')

    def clean_table_prefix(self):
        table_prefix = self.cleaned_data.get('table_prefix')
        reg = re.compile('^[a-z0-9A-Z_ ]*$')
        if not reg.match(str(table_prefix)):
            raise ValidationError('Invalid Table Prefix')
        return self.cleaned_data.get('table_prefix')

    def clean_host(self):
        host = self.cleaned_data.get('host')
        reg = re.compile('^[a-z0-9A-Z_.-]*$')
        if not reg.match(str(host)):
            raise ValidationError('Invalid Host')
        if host=="localhost":
            raise ValidationError('localhost is not allowed')
        return self.cleaned_data.get('host')
    def clean_tar_password(self):
        password = self.cleaned_data.get('tar_password')
        if password == None:
            raise ValidationError('Password Required')
        return self.cleaned_data.get('tar_password')

    def clean_port(self):
        port = self.cleaned_data.get('port')
        reg = re.compile('^[0-9]*$')
        if not reg.match(str(port)):
            raise ValidationError('Invalid Port')
        return self.cleaned_data.get('port')

    def clean_user(self):
        user = self.cleaned_data.get('user')
        reg = re.compile('^[a-zA-Z_@.-]*$')
        if not reg.match(str(user)):
            raise ValidationError('Invalid User')
        return self.cleaned_data.get('user')

    # def clean(self):
    #     enc_type = self.cleaned_data.get('enc_type')
    #     ssh_port = self.cleaned_data.get('ssh_port')
    #     ssh_user = self.cleaned_data.get('ssh_user')
    #     reg_port = re.compile('^[0-9]*$')
    #     reg_user = re.compile('^[a-zA-Z_@.-]*$')
    #     print(enc_type,"xsfdsf")
    #     print(not reg_port.match(str(ssh_port)))
    #     if enc_type == "ssh_tunnel":
    #         if not reg_port.match(str(ssh_port))==False:
    #             raise ValidationError('Invalid Port')
    #         if not reg_user.match(str(ssh_user)) or ssh_user == None:
    #             raise ValidationError('Invalid User')
    #
    # def clean_ssh_port(self):
    #     enc_type = self.cleaned_data.get('enc_type')
    #     ssh_port = self.cleaned_data.get('ssh_port')
    #     reg = re.compile('^[0-9]*$')
    #     try:
    #         if enc_type == "ssh_tunnel":
    #             if not reg.match(str(ssh_port)) or ssh_port==None:
    #                 raise ValidationError('Invalid Port')
    #
    #     except:
    #         pass
    #     return ssh_port
    #
    # def clean_ssh_user(self):
    #     enc_type = self.cleaned_data.get('enc_type')
    #     ssh_user = self.cleaned_data.get('ssh_user')
    #     reg_user = re.compile('^[a-zA-Z_@.-]*$')
    #     try:
    #         if enc_type == "ssh_tunnel":
    #             if not reg.match(str(ssh_user))or ssh_user==None:
    #                 raise ValidationError('Invalid User')
    #     except:
    #         pass
    #     return ssh_user
    #
    # def clean_remote_address(self):
    #     enc_type = self.cleaned_data.get('enc_type')
    #     remote_address = self.cleaned_data.get('remote_address')
    #     reg = re.compile('^[0-9.:]*$')
    #     try:
    #         if enc_type == "ssh_tunnel":
    #             if not reg.match(str(remote_address)) or remote_address==None:
    #                 raise ValidationError('Invalid remote address')
    #     except:
    #         pass
    #     return remote_address
    #
    # def clean_public_key(self):
    #     enc_type = self.cleaned_data.get('enc_type')
    #     public_key = self.cleaned_data.get('public_key')
    #     try:
    #         if enc_type == "ssh_tunnel":
    #             if public_key==None:
    #                 raise ValidationError('Public Key Required')
    #     except:
    #         pass
    #     return public_key



