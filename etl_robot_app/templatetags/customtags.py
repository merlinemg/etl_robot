from django import template
from etlrobot import settings
import json
from etl_robot_app.models import AccountSource,Source,Account
register = template.Library()

@register.simple_tag
def classActivation(id,srccategory):
    class_cat=""
    for srccat in srccategory:
        if id==srccat.source.id:
            class_cat=class_cat+" "+srccat.category.name

    return class_cat

@register.simple_tag
def getPageUrl(argument):
    return{
        settings.FACEBOOK_ADS:'facebookadsintegration',
        settings.FACEBOOK_INSIGHT:'facebookinsightintegration',
        settings.GOOGLE_ANALYTICS:'analyticsintegration',
        settings.PINTEREST:'pinterestintegration',
        settings.FIVE9:'five9tintegration',
        settings.MYSQL:'mysqltarget',
        settings.BIG_QUERY:'bigquerytarget',
    }[argument]

@register.simple_tag
def getFormAction(argument):
    return {
        settings.FACEBOOK_ADS: 'facebook/ads',
        settings.FACEBOOK_INSIGHT: 'pages/insights',
        settings.GOOGLE_ANALYTICS: 'google/analytics',
        settings.PINTEREST: 'pinterest/boards',
        settings.FIVE9: 'fiveNine',
    }[argument]

@register.simple_tag
def gettooltip_message(accountid,account,type):
    account= Account.objects.get(id=account)
    if type == settings.FACEBOOK_ADS:
            source = Source.objects.get(name=type)
            check_account_source = AccountSource.objects.filter(account=account,json_key__contains="'ad_account_id': " + "\'" + str(accountid) + "\'", source=source)
    elif type == settings.FACEBOOK_INSIGHT:
            source = Source.objects.get(name=type)
            check_account_source = AccountSource.objects.filter(account=account,json_key__contains="'page_id': " + "\'" + str(accountid) + "\'", source=source)
            print(check_account_source)
    elif type == settings.GOOGLE_ANALYTICS:
            source = Source.objects.get(name=type)
            check_account_source= AccountSource.objects.filter(account=account,json_key__contains="'profile_id': "+"\'"+str(accountid)+"\'",source=source)
            print(check_account_source)
    elif type == settings.PINTEREST:
            source = Source.objects.get(name=type)
            check_account_source= AccountSource.objects.filter(account=account,json_key__contains="\'"+str(accountid)+"\'",source=source)
    else:
        return "False"
    if check_account_source:
        return 'The Account is already Tracked in our system'
    else:
        return ""
@register.simple_tag
def loadjson(data):
    return json.loads(data)