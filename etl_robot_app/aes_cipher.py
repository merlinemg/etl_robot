import sys
import base64
from Crypto.Cipher import AES
from .cipher_key import aes_key

class AESCipher:
    """
    :return: utf-8 string
    """
    def __init__(self):
        # password must be padded to 16 characters
        self.BS = 16
        self.pad = lambda s: s + (self.BS - len(s) % self.BS) * chr(self.BS - len(s) % self.BS)
        self.unpad = lambda s: s[:-ord(s[len(s) - 1:])]
        self.iv = b'\x00' * 16  # initialization vector
        self.key = aes_key['key'][:24]

    def encrypt(self, pwd):
        pwd = self.pad(pwd)
        aes = AES.new(self.key, AES.MODE_CBC, self.iv)  # Cipher-Block Chaining (CBC)
        return base64.b64encode(aes.encrypt(pwd)).decode('utf-8')

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        aes = AES.new(self.key, AES.MODE_CBC, self.iv)  # Cipher-Block Chaining (CBC)
        return self.unpad(aes.decrypt(enc)).decode('utf-8')


def main():
    client = AESCipher()

    print('---encrypt---')
    raw = 'Welcome$1'
    enc = client.encrypt(raw)
    print('{} / {}'.format(raw, enc))

    print('')

    print('---decrypt---')
    enc = 'HiYf75jkhQn+CYP7UKteAw=='
    raw = client.decrypt(enc)
    print('{} / {}'.format(enc, raw))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        raise
