from django.shortcuts import render,redirect
from django.urls import reverse
from django.views import generic
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .forms import RegistrationForm,FiveNineForm,MysqlForm
from .models import User, Account, VBillingPeriod, VCalendarPeriod,Target
from .email import SendEmail
from django.http import HttpResponse,HttpResponseRedirect
from .dashboardmixin import DashboardMixin
from .integrationmixin import IntegrationMixin
import json
from etlrobot import settings
from django.contrib.sites.models import Site
import requests
import urllib
from facebookads.api import FacebookAdsApi
from facebookads.exceptions import FacebookRequestError
from facebookads.adobjects.adaccountuser import AdAccountUser
from facebookads.adobjects.adaccount import AdAccount
from facebookads.adobjects.adset import AdSet
import httplib2
from oauth2client.client import OAuth2Credentials
from googleapiclient.discovery import build
import sys
# Create your views here.

class RegistrationView(generic.View):
    def post(self,request):
        form =RegistrationForm(request.POST)
        if form.is_valid():
            account = Account(company_name=form.cleaned_data.get('company'))
            account.save()
            account.refresh_from_db()
            if account.company_name:
                create_user = User.objects.get_or_create(account_id=account,email=form.cleaned_data.get('email'),first_name=form.cleaned_data.get('first_name'),last_name =form.cleaned_data.get('last_name'))
                if create_user:
                    email_status = SendEmail.send_mail_register(create_user)
                    if(email_status):
                        return HttpResponseRedirect(reverse('etl_robot_app:registersuccess'))

            else:
                return render(request, 'register.html', {'form': form,'error_message':'Some Error Occured'})

        else:
            return render(request, 'register.html', {'form': form})

    def get(self, request):
        form = RegistrationForm()
        return render(request, 'register.html', {'form': form})

class RegistrationSuccessView(generic.TemplateView):
    #template_name = 'emailregtemplate.html'
    template_name = 'register_done.html'

@method_decorator(login_required, name='dispatch')
class DashboardView(generic.TemplateView):
    template_name = 'dashboard/dashboard.html'

    def get(self,request):
          billing_period = VBillingPeriod.objects.filter(account_id=request.user.account_id.id)
          if not billing_period:
              billing_period = VCalendarPeriod.objects.all()
          return render(request,'dashboard/dashboard.html',{'billing_period':billing_period })

@method_decorator(login_required, name='dispatch')
class RowsSyncedPeriod(generic.View,DashboardMixin):
    def post(self,request):
        result = {}
        if request.POST['period_end_utc'] and request.POST['period_start_utc']:
            data =self.get_dashboard_summary(request.user.account_id,request.POST['period_start_utc'],request.POST['period_end_utc'],False)
            result['status'] = True
            result['data'] = data
        else:
            result['status'] = False
            result['message']='Kindly Select a valid Date Range'
        return HttpResponse(json.dumps(result), content_type="application/json")


@method_decorator(login_required, name='dispatch')
class RowsSyncedToday(generic.View,DashboardMixin):
    def post(self,request):
        result = {}
        data = self.get_dashboard_summary(request.user.account_id)
        result['status'] = True
        result['data'] = data

        return HttpResponse(json.dumps(result), content_type="application/json")

@method_decorator(login_required, name='dispatch')
class ActiveIntegrationCount(generic.View,DashboardMixin):
    def post(self,request):
        result = {}
        data = self.get_dashboard_integration_count(request.user.account_id)
        result['status'] = True
        result['data'] = data

        return HttpResponse(json.dumps(result), content_type="application/json")

@method_decorator(login_required,name='dispatch')
class NotificationCount(generic.View,DashboardMixin):
    def post(self,request):
        result={}
        data = self.get_notification_count(request.user.account_id)
        result['status']=True
        result['data']=data
        return HttpResponse(json.dumps(result),content_type="application/json")

@method_decorator(login_required,name='dispatch')
class DailyVolume(generic.View,DashboardMixin):
    def post(self,request):
        result = {}
        if request.POST['period_end_utc'] and request.POST['period_start_utc']:
            data =self.get_dashboard_summary(request.user.account_id,request.POST['period_start_utc'],request.POST['period_end_utc'],flag=True)
            result['status'] = True
            result['data'] =  data

        else:
            result['status'] = False
            result['message']='Kindly Select a valid Date Range'
        return HttpResponse(json.dumps(result), content_type="application/json")

@method_decorator(login_required,name='dispatch')
class IntegrationDonutView(generic.View,DashboardMixin):
    def post(self,request):
        result = {}
        if request.POST['period_end_utc'] and request.POST['period_start_utc']:
            data = self.get_dashboard_summary(request.user.account_id, request.POST['period_start_utc'],request.POST['period_end_utc'], flag='donut')
            result['status'] = True
            result['data'] = data

        else:
            result['status'] = False
            result['message'] = 'Kindly Select a valid Date Range'
        return HttpResponse(json.dumps(result), content_type="application/json")

@method_decorator(login_required,name='dispatch')
class IntegrationSummary(generic.View,DashboardMixin):
    def post(self,request):
        result ={}
        if request.POST['period_end_utc'] and request.POST['period_start_utc']:
            data = self.get_integration_summary(request.user.account_id,request.POST['period_start_utc'],request.POST['period_end_utc'])
            result['status'] = True
            result['data'] = data
        else:
            result['status'] = False
            result['message'] = 'Kindly Select a valid Date Range'
        return HttpResponse(json.dumps(result), content_type="application/json")

@method_decorator(login_required,name='dispatch')
class IntegrationView(generic.View,IntegrationMixin):

    def get(self,request):
        category =self.get_integration_category()
        source =self.get_integration_source()
        src_category =self.get_category_integration_src()
        target=self.get_target()
        return render(request,'integration/integration.html',{'category':category,'source':source,'src_catgory':src_category,'target':target})


@method_decorator(login_required,name='dispatch')
class FacebookAdsIntegrationView(generic.View,IntegrationMixin):

    def get(self,request):
        frequency = self.get_frequency()
        source = self.get_integration_source(settings.FACEBOOK_ADS)
        time_zone = ""
        additional_option = self.get_additional_option(settings.FACEBOOK_ADS)
        if additional_option:
            time_zone =self.get_timezone_additonal_details()
        return render(request,'integration/integration-page.html',{'source':source,'frequency':frequency,'additional_option':additional_option,'time_zone':time_zone})


@method_decorator(login_required,name='dispatch')
class FacebookInsightsIntegrationView(generic.View,IntegrationMixin):

    def get(self,request):
        frequency = self.get_frequency()
        source = self.get_integration_source(settings.FACEBOOK_INSIGHT)
        time_zone = ""
        additional_option = self.get_additional_option(settings.FACEBOOK_INSIGHT)
        if additional_option:
            time_zone = self.get_timezone_additonal_details()
        return render(request, 'integration/integration-page.html',{'source': source, 'frequency': frequency, 'additional_option': additional_option,'time_zone': time_zone})

@method_decorator(login_required,name='dispatch')
class PinterstIntegrationView(generic.View,IntegrationMixin):

    def get(self,request):
        frequency = self.get_frequency()
        source = self.get_integration_source(settings.PINTEREST)
        time_zone = ""
        additional_option = self.get_additional_option(settings.PINTEREST)
        if additional_option:
            time_zone = self.get_timezone_additonal_details()
        return render(request, 'integration/integration-page.html',{'source': source, 'frequency': frequency, 'additional_option': additional_option,'time_zone': time_zone})

@method_decorator(login_required,name='dispatch')
class GoogleAnalyticsIntegrationView(generic.View,IntegrationMixin):

    def get(self,request):
        frequency = self.get_frequency()
        source = self.get_integration_source(settings.GOOGLE_ANALYTICS)
        time_zone = ""
        additional_option = self.get_additional_option(settings.GOOGLE_ANALYTICS)
        if additional_option:
            time_zone = self.get_timezone_additonal_details()
        return render(request, 'integration/integration-page.html',                     {'source': source, 'frequency': frequency, 'additional_option': additional_option,'time_zone': time_zone})

@method_decorator(login_required,name='dispatch')
class Five9IntegrationView(generic.View,IntegrationMixin):

    def get(self,request):
        frequency = self.get_frequency()
        source = self.get_integration_source(settings.FIVE9)
        time_zone = ""
        additional_option = self.get_additional_option(settings.FIVE9)
        if additional_option:
            time_zone = self.get_timezone_additonal_details()
        return render(request, 'integration/integration-page.html',{'source': source, 'frequency': frequency, 'additional_option': additional_option,'time_zone': time_zone})


@method_decorator(login_required,name='dispatch')
class FacebookAdAuthView(generic.View,IntegrationMixin):
    def post(self,request):
        facebookAdSourceArg = {}
        request.session['is_facebook_ad'] = True
        request.session['is_facebook_insight'] = False
        facebookAdSourceArg['frequency'] = request.POST.get('frequency')
        facebookAdSourceArg['source'] = request.POST.get('source')
        facebookAdSourceArg['previous_year_date'] = request.POST.get('previous_year_date')
        facebookAdSourceArg['previous_integration'] = request.POST.get('previous_integration')
        facebookAdSourceArg['custom_question_id'] = request.POST.get('custom_ques_id')
        facebookAdSourceArg['time_zone'] = request.POST.get('timezone')
        request.session['fb_ad_src_details'] = facebookAdSourceArg
        return redirect('/auth/login/facebook/?extrascope=fbads')

@method_decorator(login_required,name='dispatch')
class FacebookPageAuthView(generic.View,IntegrationMixin):
    def post(self,request):
            facebookSourceArg={}
            request.session['is_facebook_insight'] =True
            request.session['is_facebook_ad'] =False
            facebookSourceArg['frequency']=request.POST.get('frequency')
            facebookSourceArg['source']=request.POST.get('source')
            facebookSourceArg['previous_year_date']=request.POST.get('previous_year_date')
            facebookSourceArg['previous_integration']=request.POST.get('previous_integration')
            facebookSourceArg['custom_question_id'] = request.POST.get('custom_ques_id')
            facebookSourceArg['time_zone'] = request.POST.get('timezone')
            request.session['fb_insight_src_details']=facebookSourceArg
            return redirect('/auth/login/facebook/')

@method_decorator(login_required,name='dispatch')
class GoogleAuthView(generic.View,IntegrationMixin):
    def post(self,request):
        googleSourceArg = {}
        request.session['is_google_analytics'] = True
        request.session['is_google_bigquery_integration'] = False
        googleSourceArg['frequency'] = request.POST.get('frequency')
        googleSourceArg['source'] = request.POST.get('source')
        googleSourceArg['previous_year_date'] = request.POST.get('previous_year_date')
        googleSourceArg['previous_integration'] = request.POST.get('previous_integration')
        googleSourceArg['custom_question_id'] = request.POST.get('custom_ques_id')
        googleSourceArg['time_zone'] = request.POST.get('timezone')
        request.session['google_analytics_src_details'] = googleSourceArg
        return redirect('/auth/login/google-oauth2/')

@method_decorator(login_required,name='dispatch')
class PinterestAuthView(generic.View,IntegrationMixin):
    def post(self,request):
        pinterestSourceArg = {}
        request.session['is_pinterest'] = True
        pinterestSourceArg['frequency'] = request.POST.get('frequency')
        pinterestSourceArg['source'] = request.POST.get('source')
        pinterestSourceArg['previous_year_date'] = request.POST.get('previous_year_date')
        pinterestSourceArg['previous_integration'] = request.POST.get('previous_integration')
        pinterestSourceArg['custom_question_id'] = request.POST.get('custom_ques_id')
        pinterestSourceArg['time_zone'] = request.POST.get('timezone')
        request.session['pinterest_src_details'] = pinterestSourceArg
        return redirect('/auth/login/pinterest/')
@method_decorator(login_required,name='dispatch')
class Five9AuthView(generic.View,IntegrationMixin):
    def post(self,request):
        fiveSourceArg = {}
        request.session['is_five'] = True
        fiveSourceArg['frequency'] = request.POST.get('frequency')
        fiveSourceArg['source'] = request.POST.get('source')
        fiveSourceArg['previous_year_date'] = request.POST.get('previous_year_date')
        fiveSourceArg['previous_integration'] = request.POST.get('previous_integration')
        fiveSourceArg['custom_question_id'] = request.POST.get('custom_ques_id')
        fiveSourceArg['time_zone'] = request.POST.get('timezone')
        request.session['five_nine_src_details'] = fiveSourceArg
        return redirect('etl_robot_app:fiveNinedetails')

@method_decorator(login_required,name='dispatch')
class FiveNineDetailView(generic.View,IntegrationMixin):
    def get(self,request):
        form=FiveNineForm()
        return render(request, "five9/five9.html",{'form':form,"account_type": settings.FIVE9})
    def post(self,request):
        form=FiveNineForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            source_name = request.POST.get('source_name')
            account_id = ""
            status = self.create_account_source(source_name, account_id, request)
            if status==True:
                return HttpResponseRedirect(reverse('etl_robot_app:get_data_source_set'))
            else:
                return render(request, "integration/nodata.html",
                              {"account_type": settings.FIVE9, 'error_message': "No Account Was Selected"})
        else:
            return render(request, 'five9/five9.html', {'form': form, "account_type": settings.FIVE9})

@method_decorator(login_required,name='dispatch')
class PinterestDetailView(generic.View,IntegrationMixin):
        def get(self, request):
            if request.session.get('pinterest_response') != None:
                pinterest_response = request.session['pinterest_response']
                del (request.session['pinterest_response'])
                request.session['pinterest_access_token'] = pinterest_response.get('access_token')

                token_url = settings.PINTEREST_API_URL + "v1/me/boards/?access_token=" + pinterest_response.get('access_token')+"&fields=id%2Cname%2Curl"
                try:
                    response = requests.get(token_url)
                    print(response)
                    if response.status_code == 200:
                        if (response.headers['content-type'].split(
                            ';')[0] == 'application/json' or response.text[:2] == '{"'):
                            loadedjson = response.json()

                            if loadedjson.get('data'):
                                return render(request, "pinterest/pinterest_boards.html",{"details": loadedjson, "account_type": settings.PINTEREST,'account': request.user.account_id.id})
                            else:
                                return render(request, "integration/nodata.html",
                                          {"account_type": settings.PINTEREST})
                    elif response.status_code==401:
                        return render(request, "integration/nodata.html",
                                      {"account_type": settings.PINTEREST,"error_message":"You are not permitted to access that resource."})
                    else:
                        return render(request, "integration/nodata.html",
                                      {"account_type": settings.PINTEREST,"error_message":"Pinterest Call was not Successful"})

                except:
                    return render(request, "integration/nodata.html",
                                  {"account_type": settings.PINTEREST,"error_message":"Pinterest is not responding please try after sometime"})
                            # i=0
                            # for i in range(i,len(loadedjson)):
                            #     del(loadedjson['data'][i]['access_token'])



            else:
                return render(request, "integration/nodata.html", {"account_type": settings.PINTEREST})

        def post(self, request):
            if request.POST.get('source_name') != None and request.POST.getlist('checkbox') != None:
                source_name = request.POST.get('source_name')
                account_id = request.POST.getlist('checkbox')
                status = self.create_account_source(source_name, account_id, request)
                if status == True:
                    return HttpResponseRedirect(reverse('etl_robot_app:get_data_source_set'))
                else:
                    return render(request, "integration/nodata.html",
                                  {"account_type": settings.PINTEREST,
                                   'error_message': "No Account Was Selected"})
            elif request.POST.getlist('checkbox') == None:
                return render(request, "integration/nodata.html",
                              {"account_type": settings.PINTEREST, 'error_message': "No Account Was Selected"})
            else:
                return render(request, "integration/nodata.html",
                              {"account_type": settings.PINTEREST,
                               'error_message': "No Account Was Selected for Tracking"})
@method_decorator(login_required,name='dispatch')
class FBAdDetialsView(generic.View,IntegrationMixin):
    def get(self,request):
        if request.session.get('facebook_response') != None:
            fbad_details=[]
            longlived_fb_access_token = self.get_fb_longlived_token(request)
            my_accounts = FacebookAdsApi.init(settings.SOCIAL_AUTH_FACEBOOK_KEY, settings.SOCIAL_AUTH_FACEBOOK_SECRET,longlived_fb_access_token )
            me = AdAccountUser(fbid='me', api=my_accounts)
            my_account = me.get_ad_accounts()

            for record in my_account:
                try:
                    account = AdAccount(record['id'])
                    fbad_details.append(account.remote_read(fields=[AdAccount.Field.name]))
                except FacebookRequestError as e:
                    return render(request, "integration/nodata.html", {"account_type": settings.FACEBOOK_ADS,'error_message':e.api_error_message()})
            if fbad_details:
               return render(request, "facebook/facebookads.html", {"details": fbad_details,"account_type":settings.FACEBOOK_ADS,'account':request.user.account_id.id})
            else:
                return render(request, "integration/nodata.html",{"account_type":settings.FACEBOOK_ADS})
        else:
            return render(request, "integration/nodata.html",{"account_type":settings.FACEBOOK_ADS})

    def post(self, request):
        if request.POST.get('source_name') != None and request.POST.get('radio') != None:
            source_name =request.POST.get('source_name')
            account_id=request.POST.get('radio')
            status= self.create_account_source(source_name,account_id,request)
            if status==True:
                return HttpResponseRedirect(reverse('etl_robot_app:get_data_source_set'))
            else:
                return render(request, "integration/nodata.html",
                              {"account_type": settings.FACEBOOK_ADS, 'error_message': "No Account Was Selected"})
        elif request.POST.get('radio') == None:
            return render(request, "integration/nodata.html", {"account_type": settings.FACEBOOK_ADS,'error_message':"No Account Was Selected"})
        else:
            return render(request, "integration/nodata.html",
                          {"account_type": settings.FACEBOOK_ADS, 'error_message': "No Account Was Selected for Tracking"})


@method_decorator(login_required,name='dispatch')
class FBInsightDetailView(generic.View,IntegrationMixin):
    def get(self,request):
        if request.session.get('facebook_response') != None:
            longlived_fb_access_token=self.get_fb_longlived_token(request)
            token_url = settings.FACEBOOK_GRAPH_URL+"me/accounts?access_token=" + longlived_fb_access_token
            response = requests.get(token_url)
            if response.status_code == 200:
                if (response.headers['content-type'].split(
                        ';')[0] == 'application/json' or response.text[:2] == '{"'):
                    loadedjson = response.json()
                    if loadedjson.get('data'):
                        return render(request, "facebook/facebook_page_insight.html",{"details": loadedjson, "account_type": settings.FACEBOOK_INSIGHT,'account': request.user.account_id.id})
                    else:
                        return render(request, "integration/nodata.html", {"account_type": settings.FACEBOOK_INSIGHT})
                        # i=0
                        # for i in range(i,len(loadedjson)):
                        #     del(loadedjson['data'][i]['access_token'])

            else:
                return render(request, "integration/nodata.html",
                              {"account_type": settings.FACEBOOK_INSIGHT})

        else:
            return render(request, "integration/nodata.html", {"account_type": settings.FACEBOOK_INSIGHT})

    def post(self, request):
        if request.POST.get('source_name') != None and request.POST.get('radio') != None:
            source_name = request.POST.get('source_name')
            account_id = request.POST.get('radio')
            status = self.create_account_source(source_name, account_id, request)
            if status==True:
                return HttpResponseRedirect(reverse('etl_robot_app:get_data_source_set'))
            else:
                return render(request, "integration/nodata.html",
                              {"account_type": settings.FACEBOOK_INSIGHT, 'error_message': "No Account Was Selected"})
        elif request.POST.get('radio') == None:
            return render(request, "integration/nodata.html",
                          {"account_type": settings.FACEBOOK_INSIGHT, 'error_message': "No Account Was Selected"})
        else:
            return render(request, "integration/nodata.html",
                          {"account_type": settings.FACEBOOK_INSIGHT,
                           'error_message': "No Account Was Selected for Tracking"})


@method_decorator(login_required,name='dispatch')
class GoogleAnalyticsDetailView(generic.View,IntegrationMixin):
    def get(self,request):
        if request.session.get('google_response')!=None:
            profiles=[]
            domain = Site.objects.get_current().domain
            google_response =request.session['google_response']
            del (request.session['google_response'])
            request.session['ga_access_token']=google_response.get('access_token')
            request.session['ga_refresh_token']=google_response.get('refresh_token')
            credentials = OAuth2Credentials(google_response['access_token'],settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY,settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET, google_response['refresh_token'],google_response['expires_in'], domain+settings.GOOGLE_REDIRECT_URL , '')
            http = httplib2.Http()
            http = credentials.authorize(http)
            service = build('analytics', 'v3', http=http)  # this will give you the service object which you
            # can use for firing API calls
            accounts = service.management().accounts().list().execute()
            if accounts.get('items'):
                for accid in accounts.get('items'):
                    properties = service.management().webproperties().list(
                        accountId=accid.get('id')).execute()
                    if properties.get('items'):
                        for props in properties.get('items'):
                            profiles.append(service.management().profiles().list(
                            accountId=accid.get('id'),
                            webPropertyId=props.get('id')).execute())

                return render(request, "google/google_analytics.html", {"details": profiles,"account_type": settings.GOOGLE_ANALYTICS,'account':request.user.account_id.id})
            else:
                return render(request, "integration/nodata.html", {"account_type": settings.GOOGLE_ANALYTICS})
        else:
            return render(request, "integration/nodata.html", {"account_type": settings.GOOGLE_ANALYTICS})

    def post(self, request):
        if request.POST.get('source_name') != None and request.POST.get('radio') != None:
            source_name =request.POST.get('source_name')
            account_id=request.POST.get('radio')
            status= self.create_account_source(source_name,account_id,request)
            if status==True:
                return HttpResponseRedirect(reverse('etl_robot_app:get_data_source_set'))
            else:
                return render(request, "integration/nodata.html",
                              {"account_type": settings.GOOGLE_ANALYTICS, 'error_message': "No Account Was Selected"})
        elif request.POST.get('radio') == None:
            return render(request, "integration/nodata.html", {"account_type": settings.GOOGLE_ANALYTICS,'error_message':"No Account Was Selected"})
        else:
            return render(request, "integration/nodata.html",
                          {"account_type": settings.GOOGLE_ANALYTICS, 'error_message': "No Account Was Selected for Tracking"})



@method_decorator(login_required,name='dispatch')
class DataSourceSetView(generic.View,IntegrationMixin):
    def get(self,request):
        data_source_set = self.get_data_source_set(request)
        source_img_url = self.get_integration_source_by_id(request.session.get('source'))
        return render(request,'integration/integration_dataset.html',{'data_source_set':data_source_set,'source':source_img_url})
    def post(self,request):
        data_source_set = self.get_data_source_set(request)
        source_img_url = self.get_integration_source_by_id(request.session.get('source'))
        data_set_id =request.POST.getlist('dataset_id')
        if data_set_id!=None:
            status= self.create_account_dataset_source(data_set_id,request)
            if status==True:
                return HttpResponseRedirect(reverse('etl_robot_app:get_data_target'))

        else:
            return render(request, 'integration/integration_dataset.html',
                          {'data_source_set': data_source_set, 'source': source_img_url,"message":"Select Atleast One table"})



@method_decorator(login_required,name='dispatch')
class DataTargetView(generic.View,IntegrationMixin):
    def get(self,request):
        target = self.get_target()
        return render(request,'integration/target_integration.html',{'target':target})


@method_decorator(login_required,name='dispatch')
class GoogleBigQueryAuthView(generic.View,IntegrationMixin):
    def get(self,request):
        request.session['is_google_analytics'] = False
        request.session['is_google_bigquery_integration'] = True
        return redirect('/auth/login/google-oauth2/?extrascope=bigquery')


@method_decorator(login_required,name='dispatch')
class GoogleBigQueryDetailView(generic.View,IntegrationMixin):
    def get(self,request):
        if request.session.get('google_response')!=None:
            gb_response = request.session.get('google_response')
            request.session['ga_access_token'] = gb_response.get('access_token')
            request.session['ga_refresh_token'] = gb_response.get('refresh_token')
            token_url = "https://content-cloudresourcemanager.googleapis.com/v1/projects?access_token=" + gb_response.get('access_token')
            response = requests.get(token_url)
            if response.status_code == 200:
                if (response.headers['content-type'].split(
                        ';')[0] == 'application/json' or response.text[:2] == '{"'):
                    loadedjson = response.json()
                    if loadedjson:
                        source = self.get_integration_source_by_id(request.session.get('source'))

                        return render(request, "target/googlebigquery.html",{"details": loadedjson, "account_type": settings.BIG_QUERY,'source':source})
                    else:
                        return render(request, "integration/nodata.html", {"account_type": settings.BIG_QUERY})
            else:
                return render(request, "integration/nodata.html", {"account_type": settings.BIG_QUERY})

        else:
            return render(request, "integration/nodata.html", {"account_type": settings.BIG_QUERY})
    def post(self,request):
        if request.POST.get('radio')!=None:
            status = self.save_target_details(request,settings.BIG_QUERY)
            if status==True:
                return HttpResponseRedirect(reverse('etl_robot_app:integration_success'))
        else:
            target = self.get_target()
            return render(request, 'integration/target_integration.html', {'target': target})


@method_decorator(login_required,name='dispatch')
class MysqlTargetView(generic.View,IntegrationMixin):
    def get(self,request):
        source = self.get_integration_source_by_id(request.session.get('source'))
        target=  Target.objects.values('json_key').get(name=settings.MYSQL)
        target=json.loads(target['json_key'])
        return render(request,'target/mysql.html',{"account_type": settings.MYSQL,'source':source,'target':target})

    def post(self,request):
        form = MysqlForm(request.POST)
        source = self.get_integration_source_by_id(request.session.get('source'))
        target = Target.objects.values('json_key').get(name=settings.MYSQL)
        target = json.loads(target['json_key'])
        if form.is_valid():
            status = self.save_target_details(request, settings.MYSQL)
            if status.get("status")==True:
                return HttpResponseRedirect(reverse('etl_robot_app:integration_success'))
            else:
                return render(request, 'target/mysql.html',{"account_type": settings.MYSQL, 'source': source, 'target': target, 'form': form,"error_message":status.get('message')})

        else:
            return render(request, 'target/mysql.html',
                          {"account_type": settings.MYSQL, 'source': source, 'target': target,'form':form})


@method_decorator(login_required,name='dispatch')
class IntegrationSuccessView(generic.View,IntegrationMixin):
    def get(self,request):
        return render(request, 'integration/success.html')

@method_decorator(login_required,name='dispatch')
class TestDbConnectionView(generic.View,IntegrationMixin):
    def post(self,request):
        if request.is_ajax():
            status =self.do_crud_mysql(request)
            return HttpResponse(json.dumps(status), content_type="application/json")