from .models import Category,Source,SourceCategory,Target,SourceFreq,SourceCustomQuestion,VTimeZone,SourceStart,AccountSource,SourceDataset,AccountSourceCustomQuestion,CustomQuestion,AccountSourceDataset,Status,AccountTarget,AccountEtl,AccountTargetTest
from django.contrib.sites.models import Site
import requests
import datetime
import MySQLdb
from etlrobot import settings
import urllib
from .aes_cipher import AESCipher
class IntegrationMixin:
    def get_integration_category(self):
        category = Category.objects.values("id","name").filter(is_active=1)
        return category
    def get_integration_source(self,srcname=""):
        if srcname:
            source=Source.objects.filter(name=srcname,is_active=1)
        else:
            source =Source.objects.filter(is_active=1)
        return source
    def get_integration_source_by_id(self,srcid):
        if srcid:
            source=Source.objects.get(id=srcid,is_active=1)
        return source
    def get_category_integration_src(self):
        source_category = SourceCategory.objects.all()
        return source_category
    def get_target(self):
        target = Target.objects.filter(is_active=1)
        return target
    def get_frequency(self,id=""):
        if id=="":
            return SourceFreq.objects.filter(is_active=1)
        else:
            return SourceFreq.objects.get(id=id,is_active=1)

    def get_additional_option(self,srcname):
        additional_option =SourceCustomQuestion.objects.filter(source__name=srcname,is_active=1)
        return additional_option
    def get_timezone_additonal_details(self):
        time_zone = VTimeZone.objects.all()
        return time_zone


    def get_fb_longlived_token(self,request):
        facebook_insight_response = request.session.get('facebook_response')
        domain = Site.objects.get_current().domain
        del(request.session['facebook_response'])
        token_url = settings.FACEBOOK_GRAPH_URL + "oauth/access_token?redirect_uri=" + domain + settings.FACEBOOK_REDIRECT_URL + "&client_id=" + settings.SOCIAL_AUTH_FACEBOOK_KEY + "&client_secret=" + settings.SOCIAL_AUTH_FACEBOOK_SECRET + "&grant_type=fb_exchange_token&fb_exchange_token=" + \
                    facebook_insight_response['access_token']
        resp = requests.get(token_url)
        if resp.status_code == 200:
            if (resp.headers['content-type'].split(
                    ';')[0] == 'application/json' or resp.text[:2] == '{"'):
                longlived_fb_access_token = resp.json()
                request.session['longlived_fb_access_token'] = longlived_fb_access_token['access_token']
                return longlived_fb_access_token['access_token']





    def create_account_source(self,source_name, accountid,request):
        json_key ={}
        if source_name == settings.FACEBOOK_ADS:
            if request.session.get('fb_ad_src_details') != None:
                print(request.session['fb_ad_src_details'] )
                account = request.user.account_id
                fb_ad_src_details = request.session.get('fb_ad_src_details')
                request.session['source'] = fb_ad_src_details.get('source')
                source = self.get_integration_source_by_id(fb_ad_src_details.get('source'))
                frequency = self.get_frequency(fb_ad_src_details.get('frequency'))
                custom_ques_id=fb_ad_src_details.get('custom_question_id')
                timezone=fb_ad_src_details.get('time_zone')
                if not frequency:
                    frequency = source.source_freq
                if fb_ad_src_details.get('previous_year_date') != "":
                    start_date = fb_ad_src_details.get('previous_year_date')
                else:
                    start_date = datetime.date.today()
                json_key['ad_account_id'] = accountid
                if request.session.get('longlived_fb_access_token')!=None:
                    json_key['access_token'] = request.session.get('longlived_fb_access_token')
                    del(request.session['longlived_fb_access_token'])
                check_account_source = AccountSource.objects.filter(account=account,json_key__contains="'ad_account_id': " + "\'" + str(accountid) + "\'", source=source)
                del(request.session['fb_ad_src_details'])
            else:
                return False

        elif source_name == settings.FACEBOOK_INSIGHT:
            if request.session.get('fb_insight_src_details') != None:
                account = request.user.account_id
                fb_insight_src_details = request.session.get('fb_insight_src_details')
                request.session['source'] = fb_insight_src_details.get('source')
                source = self.get_integration_source_by_id(fb_insight_src_details.get('source'))
                frequency = self.get_frequency(fb_insight_src_details.get('frequency'))
                custom_ques_id = fb_insight_src_details.get('custom_question_id')
                timezone = fb_insight_src_details.get('time_zone')
                if not frequency:
                    frequency = source.source_freq
                if fb_insight_src_details.get('previous_year_date') != "":
                    start_date = fb_insight_src_details.get('previous_year_date')
                else:
                    start_date = datetime.date.today()
                json_key['page_id'] = accountid
                if request.session.get('longlived_fb_access_token'):
                    json_key['access_token'] = request.session.get('longlived_fb_access_token')
                    del (request.session['longlived_fb_access_token'])
                check_account_source = AccountSource.objects.filter(account=account,json_key__contains="'page_id': " + "\'" + str(accountid) + "\'", source=source)
                del (request.session['fb_insight_src_details'])
            else:
                return False

        elif source_name == settings.GOOGLE_ANALYTICS:
            if request.session.get('google_analytics_src_details')!=None:
                account = request.user.account_id
                ga_account_src_details = request.session.get('google_analytics_src_details')
                request.session['source'] = ga_account_src_details.get('source')
                source = self.get_integration_source_by_id(ga_account_src_details.get('source'))
                frequency =self.get_frequency(ga_account_src_details.get('frequency'))
                custom_ques_id = ga_account_src_details.get('custom_question_id')
                timezone = ga_account_src_details.get('time_zone')
                if not frequency:
                    frequency = source.source_freq
                if ga_account_src_details.get('previous_year_date')!="":
                    start_date =ga_account_src_details.get('previous_year_date')
                else:
                    start_date =datetime.date.today()
                json_key['profile_id']=accountid
                if request.session.get('ga_access_token') and request.session.get('ga_refresh_token'):
                    json_key['access_token']=request.session.get('ga_access_token')
                    json_key['refresh_token']=request.session.get('ga_refresh_token')
                    del (request.session['ga_access_token'])
                    del (request.session['ga_refresh_token'])
                # check_account_source= AccountSource.objects.filter(account=account,json_key__contains="'profile_id': "+"\'"+str(accountid)+"\'",source=source)
                # del (request.session['google_analytics_src_details'])
            else:
                return False

        elif source_name == settings.PINTEREST:
            if request.session.get('pinterest_src_details') != None:
                account = request.user.account_id
                pinterest_src_details = request.session.get('pinterest_src_details')
                request.session['source'] = pinterest_src_details.get('source')
                source = self.get_integration_source_by_id(pinterest_src_details.get('source'))
                frequency = self.get_frequency(pinterest_src_details.get('frequency'))
                custom_ques_id = pinterest_src_details.get('custom_question_id')
                timezone = pinterest_src_details.get('time_zone')
                if not frequency:
                    frequency = source.source_freq
                if pinterest_src_details.get('previous_year_date') != "":
                    start_date = pinterest_src_details.get('previous_year_date')
                else:
                    start_date = datetime.date.today()
                json_key['board_id'] = accountid
                if request.session.get('pinterest_access_token'):
                    json_key['access_token'] = request.session.get('pinterest_access_token')
                    del (request.session['pinterest_access_token'])
                # check_account_source= AccountSource.objects.filter(account=account,json_key__contains="'profile_id': "+"\'"+str(accountid)+"\'",source=source)
                # del (request.session['google_analytics_src_details'])
            else:
                return False
        elif source_name == settings.FIVE9:
            aes =AESCipher()
            account = request.user.account_id
            five_nine_src_details = request.session.get('five_nine_src_details')
            request.session['source'] = five_nine_src_details.get('source')
            source = self.get_integration_source_by_id(five_nine_src_details.get('source'))
            frequency = self.get_frequency(five_nine_src_details.get('frequency'))
            custom_ques_id = five_nine_src_details.get('custom_question_id')
            timezone = five_nine_src_details.get('time_zone')
            if not frequency:
                frequency = source.source_freq
            if five_nine_src_details.get('previous_year_date') != "":
                start_date = five_nine_src_details.get('previous_year_date')
            else:
                start_date = datetime.date.today()
            email =request.POST.get('email')
            password =request.POST.get('password')
            password=aes.encrypt(password)
            json_key['email']=email
            json_key['password']=password
        else:
            return False
        # print(account,"\n",source,"\n",start_date,'\n',json_key,"\n",frequency,"\n",ga_account_src_details.get('previous_year_date'))
        # if check_account_source:
        #     check_account_source.update(account=account,source=source,start_from_date=start_date,source_freq=frequency,json_key=json_key,updated_at=datetime.datetime.now())
        # else:
        account_source = AccountSource(account=account,source=source,start_from_date=start_date,source_freq=frequency,json_key=json_key,created_at=datetime.datetime.now(),updated_at=datetime.datetime.now())
        account_source.save()
        request.session['account_source_id']=account_source.id
        if timezone!=None and custom_ques_id!=None:
            customquestion=CustomQuestion.objects.get(id=custom_ques_id)
            account_source_obj=AccountSource.objects.get(id=account_source.id)
            acc_custom_question =AccountSourceCustomQuestion(account_source=account_source_obj,custom_question=customquestion,input_value=timezone,created_at=datetime.datetime.now(),updated_at=datetime.datetime.now())
            acc_custom_question.save()

        return True



    def get_data_source_set(self,request):
        data_source_set=[]
        if request.session.get('source')!=None:
            source= self.get_integration_source_by_id(request.session.get('source'))
            data_source_set = SourceDataset.objects.filter(source=source,is_active=1)
        return data_source_set


    def create_account_dataset_source(self,datasetid,request):
        accountsource = AccountSource.objects.get(id=request.session.get('account_source_id'))
        status_id= Status.objects.get(id=1)
        for data in datasetid:
            sourcedata_set = SourceDataset.objects.get(id=data)
            account_source_data_set = AccountSourceDataset.objects.filter(account_source=accountsource,source_dataset=sourcedata_set)
            if account_source_data_set:
                continue
            else:
                account_source_dataset=AccountSourceDataset(account_source=accountsource,source_dataset=sourcedata_set,schema_version=sourcedata_set.schema_version,status=status_id,created_at=datetime.datetime.now(),updated_at=datetime.datetime.now())
                account_source_dataset.save()
        return True

    def save_target_details(self,request,type):
        aes = AESCipher()
        json_key={}
        account =request.user.account_id
        schema_name= self.conver_str_to_lwr_space_underscore(request.POST.get('schema_name'))
        table_prefix= self.conver_str_to_lwr_space_underscore(request.POST.get('table_prefix'))
        account_source_id = AccountSource.objects.get(id=request.session.get('account_source_id'))
        target_source= Target.objects.get(name=type)
        if schema_name==None:
            schema_name= self.get_integration_source_by_id(request.session.get('source'))
        if table_prefix==None:
            table_prefix=self.get_integration_source_by_id(request.session.get('source'))

        if type==settings.BIG_QUERY:
            json_key['project_id']=request.POST.get('radio')
            json_key['refresh_token']=request.session.get('ga_refresh_token')
            json_key['access_token']=request.session.get('ga_access_token')
        elif type==settings.MYSQL:
            json_key['db'] = schema_name
            json_key['host'] = request.POST.get('host')
            json_key['user'] = request.POST.get('user')
            json_key['port'] = request.POST.get('port')
            json_key['passwd'] = aes.encrypt(request.POST.get('tar_password'))
            if request.POST.get('enc_type')==settings.ENCRYPTION_TYPE:
                json_key['ssh_port']= request.POST.get('ssh_port')
                json_key['ssh_user']= request.POST.get('ssh_user')
                json_key['remote_address']= request.POST.get('remote_address')
                json_key['etlrobot_public_key']= request.POST.get('public_key')

        account_target= AccountTarget(account_id=account,target_id=target_source,json_key=json_key,created_at=datetime.datetime.now(),updated_at=datetime.datetime.now())
        account_target.save()
        if account_target.id:
            status_id = Status.objects.get(id=1)
            account_target= AccountTarget.objects.get(id=account_target.id)
            account_etl = AccountEtl(account=account,account_source=account_source_id,account_target=account_target,schema_name=schema_name,prefix=table_prefix,status=status_id,created_at=datetime.datetime.now(),updated_at=datetime.datetime.now(),do_create_schema=0)
            account_etl.save()
            if type == settings.MYSQL:
                result={}
                response = self.do_crud_mysql(request)
                if response.get('is_connect')==1:
                    if response.get('is_create_schema')==1:
                        is_passed =1
                        acccount_target_test =AccountTargetTest(account_target=account_target,is_passed=is_passed,version=response.get('version'),can_connect=response.get('is_connect'),can_create_schema=response.get('is_create_schema'),can_select=response.get('is_select'),can_create_table=response.get('is_create_table'),can_insert=response.get('is_insert_record'),can_update=response.get('is_update'),can_delete=response.get('is_delete'),can_drop_table=response.get('is_drop_table'),can_drop_schema=response.get('is_drop_schema'),connect_error=response.get('connect_error'),create_schema_error=response.get('schema_create_error'),select_error=response.get('is_select_error'),create_table_error=response.get('table_create_error'),insert_error=response.get('insert_record_error'),update_error=response.get('is_update_error'),delete_error=response.get('is_delete_error'),drop_table_error=response.get('is_drop_table_error'),drop_schema_error=response.get('is_drop_schema_error'),created_at=datetime.datetime.now(),updated_at=datetime.datetime.now())
                        acccount_target_test.save()
                        if response.get('is_create_table')==1:
                            if response.get('is_insert_record')==1:
                                if response.get('is_select')==1:
                                    if response.get('is_update')==1:
                                        if response.get('is_delete')==1:
                                            if response.get('is_drop_table')==1:
                                                if response.get('is_drop_schema')==1:
                                                    result['status'] = True
                                                    result['message']="Success"
                                                else:
                                                    result['status'] = False
                                                    result['message'] = response.get('is_drop_schema_error')
                                            else:
                                                result['status'] = False
                                                result['message'] = response.get('is_drop_table_error')
                                        else:
                                            result['status'] = False
                                            result['message'] = response.get('is_delete_error')
                                    else:
                                        result['status'] = False
                                        result['message'] = response.get('is_update_error')
                                else:
                                    result['status'] = False
                                    result['message'] = response.get('is_select_error')
                            else:
                                result['status'] = False
                                result['message'] = response.get('is_insert_record')
                        else:
                            result['status'] = False
                            result['message'] = response.get('table_create_error')


                    else:
                        is_passed = 0
                        acccount_target_test = AccountTargetTest(account_target=account_target, is_passed=is_passed,version=response.get('version'),can_connect=response.get('is_connect'),can_create_schema=response.get('is_create_schema'),can_select=response.get('is_select'),can_create_table=response.get('is_create_table'),can_insert=response.get('is_insert_record'),can_update=response.get('is_update'),can_delete=response.get('is_delete'),can_drop_table=response.get('is_drop_table'),can_drop_schema=response.get('is_drop_schema'),connect_error=response.get('connect_error'),create_schema_error=response.get('schema_create_error'),select_error=response.get('is_select_error'),create_table_error=response.get('table_create_error'),insert_error=response.get('insert_record_error'),update_error=response.get('is_update_error'),delete_error=response.get('is_delete_error'),drop_table_error=response.get('is_drop_table_error'),drop_schema_error=response.get('is_drop_schema_error'),created_at=datetime.datetime.now(),updated_at=datetime.datetime.now())
                        acccount_target_test.save()
                        result['status'] = False
                        result['message'] = response.get('schema_create_error')


                else:
                    is_passed = 0
                    acccount_target_test = AccountTargetTest(account_target=account_target, is_passed=is_passed,version=response.get('version'),can_connect=response.get('is_connect'),can_create_schema=response.get('is_create_schema'),can_select=response.get('is_select'),can_create_table=response.get('is_create_table'),can_insert=response.get('is_insert_record'),can_update=response.get('is_update'),can_delete=response.get('is_delete'),can_drop_table=response.get('is_drop_table'),can_drop_schema=response.get('is_drop_schema'),connect_error=response.get('connect_error'),create_schema_error=response.get('schema_create_error'),select_error=response.get('is_select_error'),create_table_error=response.get('table_create_error'),insert_error=response.get('insert_record_error'),update_error=response.get('is_update_error'),delete_error=response.get('is_delete_error'),drop_table_error=response.get('is_drop_table_error'),drop_schema_error=response.get('is_drop_schema_error'),created_at=datetime.datetime.now(),updated_at=datetime.datetime.now())
                    acccount_target_test.save()
                    result['status']=False
                    result['message']=response.get('connect_error')

                return result

            else:
                return True



    def conver_str_to_lwr_space_underscore(self,text):
        text = text.lower()
        text = text.replace(" ","_")
        return text

    def do_crud_mysql(self,request):
        response={}
        password = request.POST.get('tar_password')
        host = request.POST.get('host')
        user = request.POST.get('user')
        passwd = str(password)
        port = int(request.POST.get('port'))
        try:
            db = MySQLdb.connect(host= host,user=user, passwd=passwd,port=port)
            cursor =db.cursor()
            response['is_connect']=1
            try:
                cursor.execute("drop database if exists etlrobot_test")
                cursor.execute("create database if not exists etlrobot_test CHARACTER SET=utf8mb4 COLLATE=utf8mb4_unicode_ci")
                cursor.execute("use etlrobot_test")
                cursor.execute("select version()")
                response['version'] = cursor.fetchone()
                response['is_create_schema']=1
                try:
                  cursor.execute("CREATE TABLE etlrobot_test.testing (id int not null AUTO_INCREMENT,val varchar(20) COLLATE utf8_bin,etl_imported_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,primary key (id)) ENGINE=InnoDB AUTO_INCREMENT=1 CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci")
                  response['is_create_table'] = 1

                  try:
                      cursor.execute("insert into etlrobot_test.testing (val) values ('ETLrobot')")
                      response['is_insert_record'] = 1
                      try:
                          cursor.execute("select * from etlrobot_test.testing")
                          response['is_select']=1
                          try:
                              cursor.execute("update etlrobot_test.testing set val = 'ETLrobot - data'")
                              response['is_update'] = 1
                              try:
                                  cursor.execute("delete from etlrobot_test.testing")
                                  response['is_delete']=1
                                  try:
                                      cursor.execute("drop table if exists etlrobot_test.testing")
                                      response['is_drop_table']=1
                                      try:
                                        cursor.execute("drop database if exists etlrobot_test")
                                        response['is_drop_schema']=1
                                      except (MySQLdb.Error) as e:
                                        response['is_drop_schema'] = 0
                                        response['is_drop_schema_error'] = e.args[1]
                                  except (MySQLdb.Error) as e:
                                    response['is_drop_table'] = 0
                                    response['is_drop_table_error'] = e.args[1]
                              except (MySQLdb.Error) as e:
                                  response['is_delete'] = 0
                                  response['is_delete_error'] = e.args[1]
                                  return response
                          except (MySQLdb.Error) as e:
                              response['is_update'] = 0
                              response['is_update_error'] = e.args[1]
                              return response


                      except (MySQLdb.Error) as e:
                          response['is_select'] = 0
                          response['is_select_error'] = e.args[1]
                          return response

                  except (MySQLdb.Error) as e:
                    response['is_insert_record'] = 0
                    response['insert_record_error'] = e.args[1]
                    return response
                except (MySQLdb.Error) as e:
                    response['is_create_table'] = 0
                    response['table_create_error'] = e.args[1]
                    return response


            except (MySQLdb.Error) as e:
                response['is_create_schema'] = 0
                response['schema_create_error'] = e.args[1]
                return response



        except (MySQLdb.Error) as e:
            response['is_connect']=0
            response['connect_error']=e.args[1]
            return response
        return response


