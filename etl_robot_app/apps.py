from django.apps import AppConfig


class EtlRobotAppConfig(AppConfig):
    name = 'etl_robot_app'
