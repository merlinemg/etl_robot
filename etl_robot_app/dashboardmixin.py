from django.db.models import Count,Sum,Max
from .models import AccountDatasetStat,AccountEtl,AccountNotify,AccountSource,Source
import datetime
from django.db.models import Q
class DashboardMixin:
    def get_dashboard_summary(self,account,start_date="",end_date="",flag=False):
        result = {}
        if start_date and end_date and flag=="donut":
            summary = AccountDatasetStat.objects.filter(end_at__range=(start_date, end_date),
            account_etl__account=account).values('account_etl__account_source__source__name').annotate(rows=Sum('source_count'))
            if len(summary)>0:
                for data in summary:
                    result[data['account_etl__account_source__source__name']]=data['rows']

            return result
        elif start_date and end_date and flag:
            summary = AccountDatasetStat.objects.filter(end_at__range=(start_date, end_date),
 account_etl__account=account).values('end_at').annotate(rows=Sum('source_count')).order_by('end_at')
            if len(summary)>0:
                for data in summary:
                    if data['end_at'].strftime('%d %b') in result:
                        result[data['end_at'].strftime('%d %b')]=result[data['end_at'].strftime('%d %b')]+data['rows']
                    else:
                        result[data['end_at'].strftime('%d %b')]=data['rows']
            return result
        elif start_date and end_date and not flag:
           summary = AccountDatasetStat.objects.filter(end_at__range=(start_date,end_date),account_etl__account=account).aggregate(rows=Sum('source_count'),rows_count=Count('source_count'))

        else:
            summary =AccountDatasetStat.objects.filter(end_at__gte=datetime.date.today(),account_etl__account=account).aggregate(rows=Sum('source_count'))
        return summary

    def get_notification_count(self,account):
        count = AccountNotify.objects.filter(resolved_at__isnull=True,account=account ).aggregate(notification=Count('id'))
        return count

    def get_integration_summary(self,account,start_date,end_date):
        if start_date and end_date:
 #            integration =AccountEtl.objects.filter(Q(account=account),Q(deleted_at__gte=end_date)|Q(deleted_at__isnull=True)).values('id','account_source__source__name','account_source__source__image_url_small','status__name','deleted_at').annotate(last_sync=Max('account_dataset__last_extract_time'))
 #            query_rows = AccountDatasetStat.objects.filter(end_at__range=(start_date, end_date),
 # account_etl__account=account).values('account_etl_id').annotate(rows=Sum('source_count'))
 #            notification_count=  AccountNotify.objects.filter(created_at__range=(start_date, end_date),account=account).values('account_etl').annotate(notification=Count('id'))
            integration=AccountEtl.objects.values('id','account_source__source__name','account_source__source__image_url_small','status__name','deleted_at').filter(Q(deleted_at__isnull=True )|Q(deleted_at__gte= end_date), Q(account=account) , Q(account_dataset__end_at__range=(
          start_date, end_date)) | Q(account_dataset__account_notify__created_at__range=(start_date, end_date))  | Q(account_dataset__id__isnull=True) , Q(accountnotify_accountetl_rel__id__isnull=True)).annotate(last_sync=Max('account_dataset__last_extract_time'), rows=Sum('account_dataset__source_count'),notification=Count('accountnotify_accountetl_rel__id'))
            # summary = AccountDatasetStat.objects.filter(end_at__range=(start_date, end_date),
            #                                             account_etl__account=account).aggregate(
            #     rows=Sum('source_count'), rows_count=Count('source_count'))
            # q =join_to(AccountEtl,summary,'id','account_etl_id',integration,'rows')
            # print(q.query.__str__())
            # integration = integration.values('id','account_source__source__name','account_source__source__image_url_small','status__name','deleted_at','account_etl_rel__account_etl','last_sync').annotate(rows=Sum('account_etl_rel__source_count'))
            # integration=integration.values('id','account_source__source__name','account_source__source__image_url_small','status__name','deleted_at','account_etl_rel__account_etl','last_sync','rows','account_etl_noti_rel__account_etl').annotate(notification=Count('account_etl_noti_rel__id'))
            # if len(query_rows) > 0:
            #     for data in integration:
            #         for row in query_rows:
            #             if data['id']==row['account_etl_id']:
            #                 data['rows']=row['rows']
            #             else:
            #                 data['rows']=0
            # else:
            #     for data in integration:
            #             data['rows'] = 0
            # if len(notification_count)>0:
            #     for data in integration:
            #         for count in notification_count:
            #             if data['id']==count['account_etl']:
            #                 data['notification']=count['notification']
            #             else:
            #                 data['notification']=0
            # else:
            #     for data in integration:
            #         data['notification'] = 0
            if len(integration)>0:
                for data in integration:
                    if data['rows']==None:
                        data['rows']=0
                    if data['last_sync']!= None:
                        data['last_sync']=data['last_sync'].strftime('%d %b %y %H:%I:%S')
                    else:
                        data['last_sync']="None"
            return list(integration)


    def get_dashboard_integration_count(self, account):
        count = AccountEtl.objects.filter(account=account,status__id__in=[1,4]).aggregate(integration=Count('id'))
        return count