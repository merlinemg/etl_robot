from django.db import models
from django.contrib.auth.models import  AbstractBaseUser,BaseUserManager
from datetime import datetime
from django.contrib.auth.models import PermissionsMixin
class UserCustomManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)

        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=30)
    sort_order = models.IntegerField(unique=True, blank=True, null=True)
    is_active = models.IntegerField()
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(default=datetime.now)


class Account(models.Model):
    company_name = models.CharField(max_length=100, blank=True, null=True)
    company_url = models.CharField(max_length=500, blank=True, null=True)
    country_code = models.CharField(max_length=2, blank=True, null=True)
    zone = models.ForeignKey('Zone', models.DO_NOTHING, blank=True, null=True,related_name="account_zone_rel")
    owner_user = models.ForeignKey('User', models.DO_NOTHING, blank=True, null=True,related_name="account_user_rel")
    is_test = models.IntegerField(default=0)
    plan_id = models.IntegerField(blank=True, null=True)
    plan_start_date = models.DateTimeField(blank=True, null=True)
    plan_reset_day = models.IntegerField(blank=True, null=True)
    is_sync_active = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(default=datetime.now)


class User(AbstractBaseUser,PermissionsMixin):
    objects = UserCustomManager()
    account_id = models.ForeignKey(Account, models.DO_NOTHING,null=True,related_name="user_account_rel")
    email = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    role = models.CharField(max_length=100, blank=True, null=True)
    login_count = models.SmallIntegerField(blank=True, null=True)
    terms_accepted_at = models.DateTimeField(blank=True, null=True)
    email_confirmed_at = models.DateTimeField(blank=True, null=True)
    password_changed_at = models.DateTimeField(blank=True, null=True)
    invite_user = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True,related_name="user_user_rel")
    user_priv = models.ForeignKey('UserPriv', models.DO_NOTHING, blank=True, null=True,related_name="user_prev_rel")
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(default=datetime.now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    USERNAME_FIELD = 'email'



class AccountBillingPeriod(models.Model):
    account = models.ForeignKey(Account, models.DO_NOTHING,related_name="billing_account_rel")
    plan_id = models.IntegerField()
    period_start = models.DateField()
    period_start_unix = models.BigIntegerField(blank=True, null=True)
    period_end = models.DateField()
    period_end_unix = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        unique_together = (('account', 'plan_id', 'period_start'),)


class AccountDatasetStat(models.Model):
    account_etl = models.ForeignKey('AccountEtl', models.DO_NOTHING,related_name="account_dataset")
    account_source_dataset = models.ForeignKey('AccountSourceDataset', models.DO_NOTHING,related_name="dataset_sourceset_rel")
    start_at = models.DateTimeField(blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)
    runtime_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    source_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    load_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    source_count = models.BigIntegerField(blank=True, null=True)
    load_count = models.BigIntegerField(blank=True, null=True)
    content_length = models.BigIntegerField(blank=True, null=True)
    last_extract_time = models.DateTimeField(blank=True, null=True)
    last_extract_id = models.BigIntegerField(blank=True, null=True)
    account_notify = models.ForeignKey('AccountNotify', models.DO_NOTHING, blank=True, null=True,related_name="dataset_notify_rel")
    error_fixed_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()


class AccountEtl(models.Model):
    account = models.ForeignKey(Account, models.DO_NOTHING)
    account_source = models.ForeignKey('AccountSource', models.DO_NOTHING,related_name="accountetl_accsource_rel")
    account_target = models.ForeignKey('AccountTarget', models.DO_NOTHING)
    schema_name = models.CharField(max_length=64)
    do_create_schema = models.IntegerField()
    schema_created_at = models.DateTimeField(blank=True, null=True)
    prefix = models.CharField(max_length=20, blank=True, null=True)
    status = models.ForeignKey('Status', models.DO_NOTHING,related_name="accountetl_status_rel")
    status_changed_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    account_etl_error_id = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()


class AccountEtlStat(models.Model):
    account_etl = models.ForeignKey(AccountEtl, models.DO_NOTHING,related_name="accountetlstat_accountetl_rel")
    account_source_dataset = models.ForeignKey('AccountSourceDataset', models.DO_NOTHING,related_name="accountetlstat_sourceset_rel")
    start_at = models.DateTimeField(blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)
    runtime_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    source_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    load_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    source_count = models.BigIntegerField(blank=True, null=True)
    load_count = models.BigIntegerField(blank=True, null=True)
    content_length = models.BigIntegerField(blank=True, null=True)
    last_extract_time = models.DateTimeField(blank=True, null=True)
    last_extract_id = models.BigIntegerField(blank=True, null=True)
    account_etl_error_id = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class AccountNotify(models.Model):
    notify_type = models.ForeignKey('NotifyType', models.DO_NOTHING,related_name="accnotify_notifytype_rel")
    account = models.ForeignKey(Account, models.DO_NOTHING, blank=True, null=True,related_name="accountnotify_account_rel")
    account_etl = models.ForeignKey(AccountEtl, models.DO_NOTHING, blank=True, null=True,related_name="accountnotify_accountetl_rel")
    level = models.CharField(max_length=10, blank=True, null=True)
    function_name = models.CharField(max_length=100, blank=True, null=True)
    error_code = models.IntegerField(blank=True, null=True)
    error = models.CharField(max_length=1000, blank=True, null=True)
    message = models.CharField(max_length=1000, blank=True, null=True)
    traceback = models.CharField(max_length=8000, blank=True, null=True)
    resolved_at = models.DateTimeField(blank=True, null=True)
    note = models.CharField(max_length=1000, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class AccountSource(models.Model):
    account = models.ForeignKey(Account, models.DO_NOTHING,related_name="accountsource_account_rel")
    source = models.ForeignKey('Source', models.DO_NOTHING,related_name="accountsource_source_rel")
    start_from_date = models.DateField()
    source_freq = models.ForeignKey('SourceFreq', models.DO_NOTHING,related_name="accountsource_sourcefreq_rel")
    json_key = models.TextField(blank=True, null=True)  # This field type is a guess.
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)



class AccountSourceCustomQuestion(models.Model):
    account_source = models.ForeignKey(AccountSource, models.DO_NOTHING,related_name="accsourques_accountsource_rel")
    custom_question = models.ForeignKey('CustomQuestion', models.DO_NOTHING,related_name="accsourques_customques_rel")
    input_value = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        unique_together = (('account_source', 'custom_question'),)


class AccountSourceDataset(models.Model):
    account_source = models.ForeignKey(AccountSource, models.DO_NOTHING,related_name="accsrcdatset_accountsrc_rel")
    source_dataset = models.ForeignKey('SourceDataset', models.DO_NOTHING,related_name="accsrcdatset_srcdatset_rel")
    schema_version = models.DecimalField(max_digits=8, decimal_places=1, blank=True, null=True)
    run_start_at = models.DateTimeField(blank=True, null=True)
    last_extract_time = models.DateTimeField(blank=True, null=True)
    status = models.ForeignKey('Status', models.DO_NOTHING,related_name="accsrcdatset_sat_rel")
    status_changed_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        unique_together = (('account_source', 'source_dataset'),)


class AccountTarget(models.Model):
    account_id = models.ForeignKey(Account, models.DO_NOTHING,related_name="account_account_target_rel")
    target_id =  models.ForeignKey('Target', models.DO_NOTHING,related_name="accounttarget_target_rel")
    schema_name = models.CharField(max_length=64, blank=True, null=True)
    do_create_schema = models.IntegerField(blank=True, null=True)
    schema_created_at = models.DateTimeField(blank=True, null=True)
    prefix = models.CharField(max_length=20, blank=True, null=True)
    json_key = models.TextField(blank=True, null=True)  # This field type is a guess.
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class AccountTargetTest(models.Model):
    account_target = models.ForeignKey(AccountTarget, models.DO_NOTHING, unique=True)
    is_passed = models.IntegerField()
    version = models.CharField(max_length=100, blank=True, null=True)
    can_connect = models.IntegerField()
    can_create_schema = models.IntegerField()
    can_select = models.IntegerField()
    can_create_table = models.IntegerField()
    can_insert = models.IntegerField()
    can_update = models.IntegerField()
    can_delete = models.IntegerField()
    can_drop_table = models.IntegerField()
    can_drop_schema = models.IntegerField()
    connect_error = models.CharField(max_length=1000, blank=True, null=True)
    create_schema_error = models.CharField(max_length=1000, blank=True, null=True)
    select_error = models.CharField(max_length=1000, blank=True, null=True)
    create_table_error = models.CharField(max_length=1000, blank=True, null=True)
    insert_error = models.CharField(max_length=1000, blank=True, null=True)
    update_error = models.CharField(max_length=1000, blank=True, null=True)
    delete_error = models.CharField(max_length=1000, blank=True, null=True)
    drop_table_error = models.CharField(max_length=1000, blank=True, null=True)
    drop_schema_error = models.CharField(max_length=1000, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class BillingCustomer(models.Model):
    account = models.ForeignKey(Account, models.DO_NOTHING,related_name="billcus_acc_rel")
    customer_id = models.CharField(max_length=50, blank=True, null=True)
    account_balance = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    created_unix = models.BigIntegerField(blank=True, null=True)
    currency = models.CharField(max_length=10, blank=True, null=True)
    email = models.CharField(max_length=128, blank=True, null=True)
    livemode = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class BillingInvoice(models.Model):
    invoice_id = models.CharField(max_length=50, blank=True, null=True)
    amount = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    currency = models.CharField(max_length=10, blank=True, null=True)
    attempt_count = models.IntegerField(blank=True, null=True)
    billing = models.CharField(max_length=50, blank=True, null=True)
    charge_id = models.CharField(max_length=50, blank=True, null=True)
    closed = models.IntegerField(blank=True, null=True)
    customer_id = models.CharField(max_length=50, blank=True, null=True)
    invoice_date_unix = models.BigIntegerField(blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    discount = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ending_balance = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    forgiven = models.IntegerField(blank=True, null=True)
    paid = models.IntegerField(blank=True, null=True)
    period_start_unix = models.BigIntegerField(blank=True, null=True)
    period_end_unix = models.BigIntegerField(blank=True, null=True)
    next_payment_unix = models.BigIntegerField(blank=True, null=True)
    subscription_id = models.CharField(max_length=50, blank=True, null=True)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    tax = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    tax_percent = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    total = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    plan_id = models.IntegerField(blank=True, null=True)
    plan_name = models.CharField(max_length=50, blank=True, null=True)
    trial_period_days = models.IntegerField(blank=True, null=True)
    interval_count = models.IntegerField(blank=True, null=True)
    proration = models.IntegerField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    plan_amount = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    subscription_item = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class BillingPlan(models.Model):
    plan_id = models.CharField(max_length=50)
    plan_name = models.CharField(max_length=50)
    cost = models.IntegerField()
    rows = models.IntegerField()
    integration_level = models.SmallIntegerField()
    support_level = models.SmallIntegerField()
    overage_cost = models.SmallIntegerField(blank=True, null=True)
    overage_rows = models.SmallIntegerField(blank=True, null=True)
    is_active = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class BillingSubscription(models.Model):
    subscription_id = models.CharField(max_length=50, blank=True, null=True)
    customer_id = models.CharField(max_length=50, blank=True, null=True)
    billing = models.CharField(max_length=50, blank=True, null=True)
    cancel_at_period_end = models.IntegerField(blank=True, null=True)
    created_unix = models.BigIntegerField(blank=True, null=True)
    canceled_unix = models.BigIntegerField(blank=True, null=True)
    current_period_start_unix = models.BigIntegerField(blank=True, null=True)
    current_period_end_unix = models.BigIntegerField(blank=True, null=True)
    livemode = models.IntegerField(blank=True, null=True)
    discount_id = models.CharField(max_length=50, blank=True, null=True)
    plan_id = models.CharField(max_length=50, blank=True, null=True)
    plan_amount = models.IntegerField(blank=True, null=True)
    plan_created_unix = models.BigIntegerField(blank=True, null=True)
    status = models.CharField(max_length=10, blank=True, null=True)
    tax_percent = models.IntegerField(blank=True, null=True)
    trail_start_unix = models.BigIntegerField(blank=True, null=True)
    trial_end_unix = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()


class ContactUs(models.Model):
    email = models.CharField(max_length=150)
    user = models.ForeignKey('User', models.DO_NOTHING, blank=True, null=True,related_name="contact_user_rel")
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    company = models.CharField(max_length=100, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    message = models.CharField(max_length=4000, blank=True, null=True)
    replied_at = models.DateTimeField(blank=True, null=True)
    replied_by = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class Country(models.Model):
    country_code = models.CharField(max_length=2, blank=True, null=True)
    country_name = models.CharField(max_length=45, blank=True, null=True)
    sort_order = models.IntegerField(blank=True, null=True)



class CustomQuestion(models.Model):
    question = models.CharField(max_length=200)
    input_type = models.CharField(max_length=50)
    is_active = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()


class EmailTemplate(models.Model):
    template_name = models.CharField(max_length=40, blank=True, null=True)
    template_id = models.CharField(max_length=40, blank=True, null=True)
    from_name = models.CharField(max_length=50, blank=True, null=True)
    from_email = models.CharField(max_length=100, blank=True, null=True)
    reply_to_email = models.CharField(max_length=100, blank=True, null=True)
    subject = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    is_transactional = models.IntegerField()
    substitutions = models.CharField(max_length=200, blank=True, null=True)
    is_active = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class ErrorLog(models.Model):
    proc = models.CharField(max_length=100, blank=True, null=True)
    lvl = models.CharField(max_length=50, blank=True, null=True)
    sql_state = models.CharField(max_length=10, blank=True, null=True)
    errno = models.IntegerField(blank=True, null=True)
    error_message = models.TextField(blank=True, null=True)
    section = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    key_id = models.BigIntegerField(blank=True, null=True)
    message = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)


class NotifyType(models.Model):
    notify_code = models.CharField(max_length=20, blank=True, null=True)
    notify_description = models.CharField(max_length=100, blank=True, null=True)
    sort_order = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class ReplicationType(models.Model):
    name = models.CharField(max_length=30)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class Source(models.Model):
    name = models.CharField(max_length=50)
    default_schema_name = models.CharField(max_length=64)
    example_prefix = models.CharField(max_length=20)
    class_name = models.CharField(max_length=50)
    source_start = models.ForeignKey('SourceStart', models.DO_NOTHING,related_name="src_srcstart_rel")
    module_name = models.CharField(max_length=50)
    source_freq = models.ForeignKey('SourceFreq', models.DO_NOTHING,related_name="src_srcfreq_rel")
    source_agg = models.ForeignKey('SourceAgg', models.DO_NOTHING,related_name="src_srcagg_rel")
    data_lag_days = models.SmallIntegerField()
    data_zone = models.ForeignKey('Zone', models.DO_NOTHING,related_name="src_datzone_rel")
    data_retention = models.CharField(max_length=20, blank=True, null=True)
    current_api_version = models.CharField(max_length=10, blank=True, null=True)
    api_url = models.CharField(max_length=200, blank=True, null=True)
    status_page_url = models.CharField(max_length=200, blank=True, null=True)
    image_url = models.CharField(max_length=200, blank=True, null=True)
    image_url_small = models.CharField(max_length=200, blank=True, null=True)
    info_url = models.CharField(max_length=200, blank=True, null=True)
    is_beta = models.IntegerField()
    is_coming_soon = models.IntegerField()
    is_premium = models.IntegerField()
    premium_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    is_active = models.IntegerField()
    key_template_json = models.TextField(blank=True, null=True)  # This field type is a guess.
    connecting_html = models.CharField(max_length=4000, blank=True, null=True)
    replicating_html = models.CharField(max_length=4000, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class SourceAgg(models.Model):
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=10)
    agg_value = models.DecimalField(max_digits=5, decimal_places=1)
    sort_order = models.IntegerField(unique=True, blank=True, null=True)
    is_active = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()


class SourceCategory(models.Model):
    source = models.ForeignKey(Source, models.DO_NOTHING,related_name="srccat_src_rel")
    category = models.ForeignKey(Category, models.DO_NOTHING,related_name="srccat_cat_rel")
    is_primary = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        unique_together = (('source', 'category'),)


class SourceCustomQuestion(models.Model):
    source = models.ForeignKey(Source, models.DO_NOTHING,related_name="srccusques_src_rel")
    custom_question = models.ForeignKey(CustomQuestion, models.DO_NOTHING,related_name="srccusques_cusques_rel")
    sort_order = models.IntegerField(blank=True, null=True)
    is_active = models.IntegerField(default=1)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        unique_together = (('source', 'custom_question'),)


class SourceDataset(models.Model):
    source = models.ForeignKey(Source, models.DO_NOTHING,related_name="srcdatset_src_rel")
    name = models.CharField(max_length=100)
    gen_floor = models.IntegerField(blank=True, null=True)
    schema_version = models.DecimalField(max_digits=8, decimal_places=1)
    sort_order = models.IntegerField(blank=True, null=True)
    is_default = models.IntegerField()
    is_active = models.IntegerField()
    description = models.CharField(max_length=200, blank=True, null=True)
    replication_type = models.ForeignKey(ReplicationType, models.DO_NOTHING, blank=True, null=True,related_name="srcdatset_reptype_rel")
    endpoint = models.CharField(max_length=50, blank=True, null=True)
    endpoint_url = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        unique_together = (('sort_order'),)


class SourceDatasetField(models.Model):
    source_dataset = models.ForeignKey(SourceDataset, models.DO_NOTHING,related_name="srcdatsetfld_srcdatset_rel")
    name = models.CharField(max_length=64)
    new_name = models.CharField(max_length=64, blank=True, null=True)
    position = models.SmallIntegerField(blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    key_type = models.CharField(max_length=5, blank=True, null=True)
    datatype = models.CharField(max_length=10)
    size = models.SmallIntegerField(blank=True, null=True)
    numeric_scale = models.SmallIntegerField(blank=True, null=True)
    format = models.CharField(max_length=50, blank=True, null=True)
    is_nested = models.IntegerField()
    nested_depth = models.IntegerField()
    parent_name = models.CharField(max_length=64, blank=True, null=True)
    version_added = models.CharField(max_length=10, blank=True, null=True)
    version_deprecated = models.CharField(max_length=10, blank=True, null=True)
    is_active = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class SourceFreq(models.Model):
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=10)
    freq_value = models.DecimalField(max_digits=5, decimal_places=1)
    sort_order = models.IntegerField(unique=True, blank=True, null=True)
    is_active = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()


class SourceStart(models.Model):
    name = models.CharField(max_length=50)
    historical_sync = models.CharField(max_length=50, blank=True, null=True)
    days_back = models.CharField(max_length=10)
    sort_order = models.IntegerField(unique=True, blank=True, null=True)
    is_active = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class SourceVersionUpdate(models.Model):
    source_dataset = models.ForeignKey(SourceDataset, models.DO_NOTHING,related_name="srcversionupdate_srcdatset_rel")
    schema_version = models.DecimalField(max_digits=8, decimal_places=1)
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)
    schema_update = models.CharField(max_length=4000, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class Status(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)
    sort_order = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class Target(models.Model):
    name = models.CharField(max_length=50)
    info_url = models.CharField(max_length=200, blank=True, null=True)
    is_beta = models.IntegerField()
    is_coming_soon = models.IntegerField()
    is_premium = models.IntegerField()
    is_active = models.IntegerField()
    json_key = models.TextField(blank=True, null=True)  # This field type is a guess.
    image_url = models.CharField(max_length=200, blank=True, null=True)
    image_url_small = models.CharField(max_length=200, blank=True, null=True)
    connecting_html = models.CharField(max_length=4000, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()




class Timezone(models.Model):
    zone = models.ForeignKey('Zone', models.DO_NOTHING,related_name="timezne_zone_rel")
    abbreviation = models.CharField(max_length=6)
    time_start = models.IntegerField()
    gmt_offset = models.IntegerField()
    dst = models.CharField(max_length=1)






class UserInvite(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING,related_name="usrinvite_user_rel")
    to_email = models.CharField(max_length=150)
    email_template = models.ForeignKey(EmailTemplate, models.DO_NOTHING,related_name="usrinvite_emailtemp_rel")
    sent_at = models.DateTimeField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()


class UserPriv(models.Model):
    name = models.CharField(max_length=30)
    sort_order = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()



class VAccountTarget(models.Model):
    account_id = models.IntegerField()
    company_name = models.CharField(max_length=100, blank=True, null=True)
    target_id = models.IntegerField()
    target_name = models.CharField(max_length=50)
    json_key = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField()


class VBillingPeriod(models.Model):
    account_id = models.IntegerField(blank=True, null=True)
    period_start_unix = models.BigIntegerField(blank=True, null=True)
    period_start_utc = models.DateTimeField(blank=True, null=True)
    period_start = models.DateField(blank=True, null=True)
    period_end_unix = models.BigIntegerField(blank=True, null=True)
    period_end_utc = models.DateTimeField(blank=True, null=True)
    period_end = models.DateField(blank=True, null=True)
    plan_id = models.CharField(max_length=50, blank=True, null=True)
    plan_name = models.CharField(max_length=50)



class VCalendarPeriod(models.Model):
    period_start =models.DateField(blank=True, null=True)
    period_end = models.DateField(blank=True, null=True)



class VDatasetConfig(models.Model):
    account_etl_id = models.IntegerField()
    sort_order = models.IntegerField(blank=True, null=True)
    account_source_dataset_id = models.IntegerField()
    dataset_name = models.CharField(max_length=100)
    current_version = models.DecimalField(max_digits=8, decimal_places=1)
    schema_version = models.DecimalField(max_digits=8, decimal_places=1, blank=True, null=True)
    last_extract_time = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()


class VDatasetConfigAll(models.Model):
    account_etl_id = models.IntegerField()
    sort_order = models.IntegerField(blank=True, null=True)
    account_source_dataset_id = models.IntegerField()
    dataset_name = models.CharField(max_length=100)
    current_version = models.DecimalField(max_digits=8, decimal_places=1)
    schema_version = models.DecimalField(max_digits=8, decimal_places=1, blank=True, null=True)
    last_extract_time = models.DateTimeField(blank=True, null=True)
    is_active = models.IntegerField()
    etl_status_id = models.IntegerField()
    asd_status_id = models.IntegerField()
    created_at = models.DateTimeField()



class VDatasetError(models.Model):
    account_dataset_stat_id = models.BigIntegerField()
    account_etl_id = models.IntegerField()
    company_name = models.CharField(max_length=100, blank=True, null=True)
    target_name = models.CharField(max_length=50)
    dataset_name = models.CharField(max_length=100)
    account_notify_id = models.BigIntegerField(blank=True, null=True)
    error = models.CharField(max_length=1000, blank=True, null=True)
    created_at = models.DateTimeField()


class VDatasetStat(models.Model):
    account_dataset_stat_id = models.BigIntegerField()
    account_etl_id = models.IntegerField()
    company_name = models.CharField(max_length=100, blank=True, null=True)
    target_name = models.CharField(max_length=50)
    account_source_dataset_id = models.IntegerField()
    dataset_name = models.CharField(max_length=100)
    start_at = models.DateTimeField(blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)
    runtime_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    load_secs = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    source_count = models.BigIntegerField(blank=True, null=True)
    load_count = models.BigIntegerField(blank=True, null=True)
    content_length = models.BigIntegerField(blank=True, null=True)
    last_extract_time = models.DateTimeField(blank=True, null=True)
    account_notify_id = models.BigIntegerField(blank=True, null=True)
    error = models.CharField(max_length=1000, blank=True, null=True)
    created_at = models.DateTimeField()



class VEtlConfig(models.Model):
    account_etl_id = models.IntegerField()
    account_id = models.IntegerField()
    company_name = models.CharField(max_length=100, blank=True, null=True)
    country_code = models.CharField(max_length=2, blank=True, null=True)
    source_name = models.CharField(max_length=50)
    default_schema_name = models.CharField(max_length=64)
    data_lag_days = models.SmallIntegerField()
    module_name = models.CharField(max_length=50)
    class_name = models.CharField(max_length=50)
    account_source_id = models.IntegerField()
    start_from_date = models.DateField()
    source_json_key = models.DateField(blank=True, null=True)
    target_id = models.IntegerField()
    target_name = models.CharField(max_length=50)
    account_target_id = models.IntegerField()
    schema_name = models.CharField(max_length=64)
    do_create_schema = models.IntegerField()
    schema_created_at = models.DateTimeField(blank=True, null=True)
    prefix = models.CharField(max_length=20, blank=True, null=True)
    target_json_key = models.CharField(max_length=400, blank=True, null=True)
    created_at = models.DateTimeField()



class VEtlSettings(models.Model):
    account_etl_id = models.IntegerField()
    account_id = models.IntegerField()
    company_name = models.CharField(max_length=100, blank=True, null=True)
    source_name = models.CharField(max_length=50)
    account_source_id = models.IntegerField()
    start_date = models.DateField()
    frequency = models.CharField(max_length=10)
    target_id = models.IntegerField()
    destination = models.CharField(max_length=50)
    account_target_id = models.IntegerField()
    schema_name = models.CharField(max_length=64)
    prefix = models.CharField(max_length=20, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()



class VGetColumns(models.Model):
    column_name = models.CharField(max_length=66)
    table_name = models.CharField(max_length=64)
    on_dup = models.CharField(max_length=141)
    etl = models.CharField(max_length=87)
    column_comment = models.CharField(max_length=1024)



class VRunning(models.Model):
    account_source_dataset_id = models.IntegerField()
    account_id = models.IntegerField()
    company_name = models.CharField(max_length=100, blank=True, null=True)
    source_id = models.IntegerField()
    source_name = models.CharField(max_length=50)
    source_dataset_id = models.IntegerField()
    dataset_name = models.CharField(max_length=100)
    schema_version = models.DecimalField(max_digits=8, decimal_places=1, blank=True, null=True)
    run_start_at = models.DateTimeField(blank=True, null=True)
    last_extract_time = models.DateTimeField(blank=True, null=True)
    status_id = models.IntegerField()
    updated_at = models.DateTimeField()



class VSourceDatasetDoc(models.Model):
    source_id = models.IntegerField()
    source_name = models.CharField(max_length=50)
    source_dataset_id = models.IntegerField()
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True, null=True)
    replication_type = models.CharField(max_length=30)
    endpoint = models.CharField(max_length=50, blank=True, null=True)
    sort_order = models.IntegerField(blank=True, null=True)



class VSourceDatasetFieldDoc(models.Model):
    source_dataset_id = models.IntegerField()
    field_name = models.CharField(max_length=64)
    new_name = models.CharField(max_length=64, blank=True, null=True)
    datatype = models.CharField(max_length=10)
    description = models.CharField(max_length=200, blank=True, null=True)
    position = models.SmallIntegerField(blank=True, null=True)




class VSourceDoc(models.Model):
    source_id = models.IntegerField()
    source_name = models.CharField(max_length=50)
    image_url_small = models.CharField(max_length=200, blank=True, null=True)
    category = models.CharField(max_length=30, blank=True, null=True)
    availability = models.CharField(max_length=7)
    default_historical_sync = models.CharField(max_length=50, blank=True, null=True)
    data_retention = models.CharField(max_length=20, blank=True, null=True)
    default_timezone = models.CharField(max_length=35, blank=True, null=True)
    api_status_page = models.CharField(max_length=200, blank=True, null=True)
    api_version = models.CharField(max_length=10, blank=True, null=True)


class VTimeZone(models.Model):
    zone_id = models.IntegerField()
    zone_name = models.CharField(max_length=35)
    zone_name_country = models.CharField(max_length=40)



class Zone(models.Model):
    zone_id = models.AutoField(primary_key=True)
    country_code = models.CharField(max_length=2)
    zone_name = models.CharField(max_length=35)
    is_active = models.IntegerField()

