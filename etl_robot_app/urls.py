from django.conf.urls import url,include
from .import views
from django.contrib.auth import views as auth_views

app_name='etl_robot_app'
urlpatterns = [
url(r'^login/$', auth_views.login,{'template_name': 'login.html'}, name='login'),
url(r'^logout/$', auth_views.logout, {'template_name': 'login.html'}, name='logout'),
url(r'register', views.RegistrationView.as_view(), name='register'),
url(r'^success', views.RegistrationSuccessView.as_view(), name='registersuccess'),
url(r'dashboard', views.DashboardView.as_view(), name='dashboard'),
url(r'rows/today', views.RowsSyncedToday.as_view(), name='today_row_count'),
url(r'rows/period', views.RowsSyncedPeriod.as_view(), name='period_row_count'),
url(r'notification/count', views.NotificationCount.as_view(), name='notification_count'),
url(r'integrationactive/count', views.ActiveIntegrationCount.as_view(), name='active_integrations'),
url(r'daily/volume', views.DailyVolume.as_view(), name='get_daily_volume'),
url(r'integration/donut', views.IntegrationDonutView.as_view(), name='get_integration_donut'),
url(r'integration/summary', views.IntegrationSummary.as_view(), name='get_integration_summary'),
url(r'dataintegration', views.IntegrationView.as_view(), name='integrations'),
url(r'facebookinsightintegration', views.FacebookInsightsIntegrationView.as_view(), name='fbinsightintegrations'),
url(r'facebookadsintegration', views.FacebookAdsIntegrationView.as_view(), name='facebookadsintegration'),
url(r'analyticsintegration', views.GoogleAnalyticsIntegrationView.as_view(), name='analyticsintegration'),
url(r'pinterestintegration', views.PinterstIntegrationView.as_view(), name='pinterestintegration'),
url(r'five9tintegration', views.Five9IntegrationView.as_view(), name='five9tintegration'),
url(r'facebook/ads', views.FacebookAdAuthView.as_view(), name='facebookadsrcintegration'),
url(r'pages/insights', views.FacebookPageAuthView.as_view(), name='facebookinsightsrcintegration'),
url(r'google/analytics', views.GoogleAuthView.as_view(), name='analyticssrcintegration'),
url(r'pinterest/boards', views.PinterestAuthView.as_view(), name='pinterestsrcintegration'),
url(r'fiveNine', views.Five9AuthView.as_view(), name='five9srctintegration'),
url(r'five9details', views.FiveNineDetailView.as_view(), name='fiveNinedetails'),
url(r'fbaddetails', views.FBAdDetialsView.as_view(), name='fbaddetails'),
url(r'fbinsightdetails', views.FBInsightDetailView.as_view(), name='fbinsightdetails'),
url(r'googleanalyticsdetails', views.GoogleAnalyticsDetailView.as_view(), name='googleanalyticsdetails'),
url(r'pinterestdetails', views.PinterestDetailView.as_view(), name='pinterestdetails'),
url(r'datasourceset', views.DataSourceSetView.as_view(), name='get_data_source_set'),
url(r'datatarget', views.DataTargetView.as_view(), name='get_data_target'),
url(r'mysqltarget', views.MysqlTargetView.as_view(), name='mysqltarget'),
url(r'bigquerytarget', views.GoogleBigQueryAuthView.as_view(), name='bigquerytarget'),
url(r'googlebigquerydetails', views.GoogleBigQueryDetailView.as_view(), name='googlebigquerydetails'),
url(r'integrationsuccess', views.IntegrationSuccessView.as_view(), name='integration_success'),
url(r'testdbconnection', views.TestDbConnectionView.as_view(), name='testdbconnection'),
#
]